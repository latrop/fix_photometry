#! /usr/bin/env python3

import os
import time
import json
from pathlib import Path
from PIL import Image
import argparse
import subprocess
import paramiko
import socket
import shutil
import numpy as np
import matplotlib.pyplot as plt
import random
from matplotlib import patches
from astropy.io import fits
import matplotlib as mpl
from libs import se


mpl.rcParams["text.usetex"] = False


def get_sex_name():
    child = subprocess.Popen(["which", "sex"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child.communicate()[0]
    rc = child.returncode
    if rc == 0:
        return "sex"
    child = subprocess.Popen(["which", "sextractor"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child.communicate()[0]
    rc = child.returncode
    if rc == 0:
        return "sextractor"
    print("SExtractor is not found.")
    exit()


class PhotFixer(object):
    def __init__(self, tile, cand_idx, band_list):
        self.tile = tile
        self.cand_idx = cand_idx
        self.deblend_mincont = 0.01
        self.deblend_nthresh = 32
        self.threshold = 3
        self.mark_unreliable = {band: False for band in band_list}
        self.sex_name = get_sex_name()
        # Initialize plotting system
        self.fig, self.ax_list = plt.subplots(ncols=len(band_list), nrows=2, figsize=(20, 10))
        self.fig.subplots_adjust(hspace=0.05, wspace=0.05, bottom=0.05)
        self.cid = self.fig.canvas.mpl_connect('button_press_event', self.onclick)
        self.kid = self.fig.canvas.mpl_connect('key_press_event', self.keyboard_event)
        self.contour_instances = []
        self.filled_instances = []

        # Load original posang
        json_file = "work_dir/candidate_%i_r.json" % cand_idx
        with open(json_file, "r") as fin:
            old_phot_data = json.load(fin)
        old_posang = old_phot_data["THETA_IMAGE"]

        # Load data in one band to determine image size
        self.band_list = band_list
        data = fits.getdata("work_dir/candidate_%i_%s.fits" % (self.cand_idx, self.band_list[0]))
        self.orig_data_size_y, self.orig_data_size_x = data.shape
        self.target_x = self.orig_data_size_x / 2
        self.target_y = self.orig_data_size_y / 2

        # Load jpeg data
        img = Image.open("work_dir/candidate_%i.jpg" % cand_idx)
        img = img.rotate(old_posang, expand=False)
        img.save("tmp.jpg")
        jpg_data = plt.imread("tmp.jpg")
        self.jpg_size = jpg_data.shape[0]
        # Crop jpeg to fits size
        pad_width = int((self.jpg_size-self.orig_data_size_x) / 2)
        jpg_data = jpg_data[pad_width:pad_width+self.orig_data_size_x,
                            pad_width:pad_width+self.orig_data_size_y]

        # Load important stuff from the headers
        self.gain = {}
        self.mag_zpt = {}
        for band in self.band_list:
            header = fits.getheader("work_dir/candidate_%i_%s.fits" % (self.cand_idx, band))
            exptime = float(header['EXPTIME'])
            self.gain[band] = float(header['CELL.GAIN'])
            self.mag_zpt[band] = 25 + 2.5 * np.log10(exptime)

        # Make a segmentation map
        self.make_seg_map()
        self.get_ellipse_params()

        # Show jpeg image
        for idx, band in enumerate(self.band_list):
            self.ax_list[0][idx].imshow(jpg_data, origin='lower')
            self.ax_list[0][idx].set_title(band)

        # Show grayscale image
        for idx, band in enumerate(self.band_list):
            data = np.flipud(fits.getdata("work_dir/candidate_%i_%s.fits" % (self.cand_idx, band)))
            data[np.isnan(data)] = 0.0
            self.ax_list[1][idx].imshow(data, origin='lower', vmin=np.percentile(data, 1),
                                        vmax=np.percentile(data, 99), cmap="gray")
        self.make_plot()

    def make_seg_map(self):
        self.magnitude = {}
        self.catalogue = {}
        self.segm_data = {}
        self.selected_segments = {}
        for band in self.band_list:
            for thresh in np.arange(self.threshold, 0.49, -0.5):
                # We run sextractor with decreasing threshold repeatedly untill there are some
                # detected objects on the image
                fname = "work_dir/candidate_%i_%s.fits" % (self.cand_idx, band)
                callString = "%s %s -c ./libs/default.sex " % (self.sex_name, fname)
                callString += "-PIXEL_SCALE 0.25 "
                callString += "-CHECKIMAGE_TYPE  SEGMENTATION "
                callString += "-CHECKIMAGE_NAME  segm_%s.fits " % (band)
                callString += "-DETECT_THRESH %i -ANALYSIS_THRESH %i  " % (thresh, thresh)
                callString += "-DEBLEND_MINCONT %1.5f " % self.deblend_mincont
                callString += "-DEBLEND_NTHRESH %i " % self.deblend_nthresh
                callString += "-MAG_ZEROPOINT %1.3f " % self.mag_zpt[band]
                callString += "-GAIN %1.3f" % self.gain[band]
                subprocess.call(callString, shell=True)
                self.catalogue[band] = se.SExCatalogue("field.cat")
                if len(self.catalogue[band]) != 0:
                    # We have detected image. Stop calling sextractor
                    break
            else:
                # No objects were detected even with vert low threshold
                print("No objects detected")
                exit()

            # Load segmentation data
            self.segm_data[band] = np.flipud(fits.getdata("segm_%s.fits" % band))
            # Find segment of a main galactic body
            main_object = self.catalogue[band].find_nearest(self.target_x, self.target_y)
            main_object_idx = int(main_object["NUMBER"])
            self.selected_segments[band] = main_object_idx
            self.magnitude[band] = main_object["MAG_PETRO"]

    def make_plot(self):
        """Show contours on the plot"""
        # Remove existing contours first
        while len(self.filled_instances) > 0:
            self.filled_instances.pop().remove()

        segm_to_show = {}
        for band_idx, band in enumerate(self.band_list):
            # Show new contours
            segm_to_show[band] = np.ones_like(self.segm_data[band], dtype=float)
            segm_to_show[band][self.segm_data[band] == 0] *= np.nan
            for obj_idx in set(self.segm_data[band].flatten()):
                if obj_idx == 0:
                    continue
                if obj_idx == self.selected_segments[band]:
                    segm_to_show[band][self.segm_data[band] == obj_idx] += 101
                else:
                    segm_to_show[band][self.segm_data[band] == obj_idx] += 124

            for row in (0, 1):
                ax = self.ax_list[row][band_idx]
                self.filled_instances.append(ax.matshow(segm_to_show[band], alpha=0.45,
                                                        cmap="autumn", vmin=100,
                                                        vmax=125, origin='lower'))

                # Show ellipse
                e = ax.add_artist(patches.Ellipse(xy=(self.ell_x[band], self.orig_data_size_y-self.ell_y[band]),
                                                  width=self.ell_a[band]*self.ell_petro[band],
                                                  height=self.ell_b[band]*self.ell_petro[band],
                                                  angle=-self.ell_pa[band],
                                                  edgecolor="c",
                                                  fill=False))
                self.filled_instances.append(e)

            if self.mark_unreliable[band] is True:
                self.ax_list[0][band_idx].set_title("%s (mag=%1.2f) [UNRELIABLE]" % (band, self.magnitude[band]))
            else:
                self.ax_list[0][band_idx].set_title("%s (mag=%1.2f)" % (band, self.magnitude[band]))

        title = "Deblending=%1.5f  nthresh=%i thresh=%1.1f " % (self.deblend_mincont, self.deblend_nthresh,
                                                                self.threshold)
        self.fig.suptitle(title)
        for i in range(len(self.band_list)):
            for j in (0, 1):
                self.ax_list[j][i].set_xticks([])
                self.ax_list[j][i].set_yticks([])
        self.fig.canvas.draw()

    def get_ellipse_params(self):
        self.phot_object = {}
        self.ell_a = {}
        self.ell_b = {}
        self.ell_x = {}
        self.ell_y = {}
        self.ell_pa = {}
        self.ell_ell = {}
        self.ell_petro = {}
        for band in self.band_list:
            self.phot_object = self.catalogue[band].find_nearest(self.target_x, self.target_y)
            self.ell_a[band] = self.phot_object["A_IMAGE"]
            self.ell_b[band] = self.phot_object["B_IMAGE"]
            self.ell_x[band] = self.phot_object["X_IMAGE"]
            self.ell_y[band] = self.phot_object["Y_IMAGE"]
            self.ell_pa[band] = self.phot_object["THETA_IMAGE"]
            self.ell_ell[band] = self.phot_object["ELLIPTICITY"]
            self.ell_petro[band] = self.phot_object["PETRO_RADIUS"]

    def onclick(self, event):
        if (event.xdata is None) or (event.ydata is None):
            return
        print(event.xdata, event.ydata)
        self.target_x = int(event.xdata)
        self.target_y = self.orig_data_size_y - int(event.ydata)
        # idx = self.segm_data[j, i]
        # if event.button == 1:
        #     # Left mouse button: add region
        #     if (idx != 0) and (idx not in self.selected_segments):
        #         self.selected_segments.append(idx)
        # if event.button == 3:
        #     # Right mouse button: remove region
        #     if (idx != 0) and (idx in self.selected_segments):
        #         self.selected_segments.remove(idx)
        self.make_seg_map()
        self.get_ellipse_params()
        self.make_plot()

    def keyboard_event(self, event):
        all_events = ["up", "down", "left", "right", " ", "[", "]", "a"]
        all_events.extend(self.band_list)
        if event.key not in all_events:
            return
        if event.key == "up":
            if self.deblend_mincont < 0.5:
                self.deblend_mincont *= 2
        if event.key == "down":
            if self.deblend_mincont > 0.000001:
                self.deblend_mincont /= 2

        if event.key == "left":
            if self.deblend_nthresh >= 4:
                self.deblend_nthresh //= 2
        if event.key == "right":
            if self.deblend_nthresh <= 32:
                self.deblend_nthresh *= 2

        if event.key == "[":
            if self.threshold > 1:
                self.threshold -= 0.5
        if event.key == "]":
            if self.threshold < 3:
                self.threshold += 0.5

        if event.key in self.band_list:
            self.mark_unreliable[event.key] = not self.mark_unreliable[event.key]
            self.make_plot()
            return

        if event.key == " ":
            for band in self.band_list:
                self.mark_unreliable[band] = not self.mark_unreliable[band]
            self.make_plot()
            return

        if event.key == "a":
            self.try_auto_phot()
            return

        self.make_seg_map()
        self.get_ellipse_params()
        self.make_plot()

    def try_auto_phot(self):
        for _ in range(15):
            self.deblend_nthresh = random.choice([2, 4, 8, 16, 32, 64])
            self.deblend_mincont = random.uniform(1e-5, 0.5)
            self.make_seg_map()
            self.get_ellipse_params()
            if self.validate_photometry() is True:
                break
        self.make_plot()

    def validate_photometry(self):
        pa_median = np.mean(list(self.ell_pa.values()))
        max_pa_diff = 2.5
        for band in self.band_list:
            if abs(self.ell_pa[band] - pa_median) > max_pa_diff:
                return False
        return True

    def save_phot_data(self, out_name=None):
        out_dir = Path("results")
        if not out_dir.exists():
            os.makedirs(str(out_dir))
        params = {}
        params["DEBLEND_NTHRESH"] = self.deblend_nthresh
        params["DEBLEND_MINCONT"] = self.deblend_mincont
        params["UNRELIABLE"] = self.mark_unreliable
        params["target_x"] = self.target_x
        params["target_y"] = self.target_y
        params["threshold"] = self.threshold
        out_name = out_dir / ("%s_%i_phot_params.json" % (self.tile, self.cand_idx))
        with open(str(out_name), "w") as fout:
            json.dump(params, fout)
        print("Data saved to %s" % out_name)


def http_proxy_tunnel_connect(proxy, target, timeout=None):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(timeout)
    sock.connect(proxy)
    cmd_connect = "CONNECT %s:%d HTTP/1.1\r\n\r\n" % target
    sock.sendall(cmd_connect.encode())
    response = []
    sock.settimeout(2)  # quick hack - replace this with something better performing.
    while True:
        chunk = sock.recv(1024).decode()
        if not chunk:  # if something goes wrong
            break
        response.append(chunk)
        if "\r\n\r\n" in chunk:  # we do not want to read too far ;)
            break
    response = ''.join(response)
    return sock


def get_next_candidate(test, proxy=None, port=None):
    """
    Go to a remote server to get new portion of tiles
    """
    pwd = open("data.dat").readlines()[0].strip()
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    if proxy is not None:
        sock = http_proxy_tunnel_connect(proxy=(proxy, port),
                                         target=("observ.astro.spbu.ru", 22),
                                         timeout=50)
        ssh.connect(hostname='observ.astro.spbu.ru', sock=sock, username='panneur', password=pwd)
    else:
        ssh.connect(hostname='observ.astro.spbu.ru', username='panneur', password=pwd)

    file_obtained = False
    while not file_obtained:
        if test is not True:
            stdin, stdout, stderr = ssh.exec_command('./dispencer.py %s' % os.uname().nodename)
        else:
            stdin, stdout, stderr = ssh.exec_command('./dispencer.py %s --test' % os.uname().nodename)
        output = stdout.read().splitlines()
        for butes in output:
            line = butes.decode("utf-8")
            if "Empty" in line:
                print("All done")
                exit()
            if "Busy" in line:
                print("The server us busy. Waiting for 5 seconds and trying again...")
                time.sleep(5)
                break
            if "Tile" in line:
                projcell, skycell, candidate, path_to_file = line.split()
                projcell = int(projcell)
                skycell = int(skycell)
                candidate = int(candidate)
                file_obtained = True
    sftp = ssh.open_sftp()
    try:
        print("Downloading data.")
        sftp.get(path_to_file, 'candidate.zip')
    except FileNotFoundError:
        return
    if not Path("candidate.zip").exists():
        # Something went wrong
        # TODO: Show error message
        return
    # Close connection
    sftp.close()
    ssh.close()
    shutil.unpack_archive("candidate.zip", extract_dir="work_dir")
    return projcell, skycell, candidate


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--projcell", type=int, default=-1)
    parser.add_argument("--skycell", type=int, default=-1)
    parser.add_argument("--candidate", type=int, default=-1)
    parser.add_argument("--test", action='store_true', default=False,
                        help="Process a rangom galaxy, but do not save results (for testing purposes).")
    parser.add_argument("--proxy", help="Proxy address, if needed.")
    parser.add_argument("--port", help="Proxy port, if needed.", type=int, default=None)
    args = parser.parse_args()
    band_list = "grizy"
    if args.projcell == -1:
        projcell, skycell, candidate = get_next_candidate(args.test, args.proxy, args.port)
    else:
        projcell, skycell, candidate = args.projcell, args.skycell, args.candidate
        path_to_data = Path("../results_combined/combined_results/new_candidates/Tile_%04i_%03i" % (projcell, skycell))
        os.makedirs("work_dir", exist_ok=True)
        shutil.copy(path_to_data / ("candidate_%i.jpg" % candidate),
                    "work_dir/candidate_%i.jpg" % candidate)
        for band in band_list:
            shutil.copy(path_to_data / ("candidate_%i_%s.fits" % (candidate, band)),
                        "work_dir/candidate_%i_%s.fits" % (candidate, band))
        shutil.copy(path_to_data / ("candidate_%i_r.json" % candidate),
                    "work_dir/candidate_%i_r.json" % candidate)

    print("Tile_%04i_%03i_%i" % (projcell, skycell, candidate))
    ph = PhotFixer(tile="Tile_%04f_%03i" % (projcell, skycell), cand_idx=candidate, band_list=band_list)
    plt.show()
    if args.test is not True:
        ph.save_phot_data()
    shutil.rmtree("work_dir")
    os.remove("field.cat")
    os.remove("tmp.jpg")
    os.remove("candidate.zip")
    for band in band_list:
        os.remove("segm_%s.fits" % band)
