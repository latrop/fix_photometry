#! /usr/bin/env python3

import os
from os import path
import shelve
import argparse
import datetime


database_file = "tiles.db"


def make_db():
    tiles = []
    for line in open("pbm.list"):
        if line.startswith("#"):
            continue
        projcell, skycell, candidate = [int(_) for _ in line.split("|")[:3]]
        tiles.append((projcell, skycell, candidate))
    db = shelve.open(database_file)
    db['tiles'] = tiles
    db.close()


def dispense_images(name):
    db = shelve.open(database_file)
    tiles = db['tiles']
    projcell, skycell, candidate = tiles.pop()
    db["tiles"] = tiles
    db.close()
    fout = open("dispencer.log", "a")
    t_string = datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")
    fout.write(f"{t_string} {name} {projcell} {skycell} {candidate}\n")
    return projcell, skycell, candidate


def main(name):
    # Do now allow for more than one instance of the script to run simultaneously
    try:
        os.open("lock", os.O_CREAT | os.O_EXCL)
    except FileExistsError:
        print("Busy")
        exit()

    # Check of the database created
    if not path.exists(database_file):
        # If not, create one
        make_db()

    # Dispense images
    projcell, skycell, candidate = dispense_images(name)
    print(projcell, skycell, candidate)

    # unlock the script
    os.remove("lock")



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("name", help="Caller's name")
    args = parser.parse_args()
    main(args.name)
